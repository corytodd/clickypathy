# ClickyPathy

ClickyPathy is a simple, right-click context menu tool to get the absolute path of a file on your clipboard.
Obviously this only handles files (not directories, drives, ln -s, etc)

- - -
## Requirements

  - Windows 7 or better
  - .NET Framework 4.0 or better
  - 32 or 64 bit supported in same packaging

- - -

## Installation 

ClickyPathy may run as a right-click context menu application or as a traditional application.

_If you are already running a similar tool developed by me, copy these libs into a v2 folder (C:\lib\v2) and
modify the install/uninstall scripts accordingly. Older tools might not use the same libraries which could cause
explorer to crash!_

### Context Menu Integration

 1. [Download](https://bitbucket.org/corytodd/clickypathy/downloads/lib.7z)
 2. Extract to C:\ so the files reside in C:\lib
 3. As **administrator**, **from the command line**, run the install batch file.
 4. Test by right clicking a file to see if a new entry for ClickyPathy is present.
 

- - -
## Usage
### Context Menu Integration
Right click any file and then ctrl+v into a your target application. That's it.
