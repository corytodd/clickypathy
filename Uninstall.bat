@echo off
goto check_Permissions

:check_Permissions
    echo Administrative permissions required. Detecting permissions...

    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo Success: Administrative permissions confirmed. Proceeding with shell extension installer
		goto CheckOs
    ) else (
        echo Failure: Current permissions inadequate. Please run as adminstrator.
		Goto End
    )

    pause >nul
	

:CheckOS
IF EXIST "%PROGRAMFILES(X86)%" (GOTO 64BIT) ELSE (GOTO 32BIT)

:64BIT
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\RegAsm C:\lib\ClickyPathy.dll /codebase /unregister
GOTO END

:32BIT
C:\Windows\Microsoft.NET\Framework\v4.0.30319\RegAsm C:\lib\ClickyPathy.dll /codebase /unregister
GOTO END

:END

