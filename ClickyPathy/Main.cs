/*
 * Copyright (C) 2014 Cory Todd <corneliustodd@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Linq;
namespace ClickyPathy
{
    [System.Runtime.InteropServices.ComVisible(true)]
    [SharpShell.Attributes.COMServerAssociation(SharpShell.Attributes.AssociationType.AllFiles)]
    public class Main : SharpShell.SharpContextMenu.SharpContextMenu
    {
        protected override bool CanShowMenu() { return true; }

        protected override System.Windows.Forms.ContextMenuStrip CreateMenu()
        {
            var menu = new System.Windows.Forms.ContextMenuStrip();
            var handleData = new System.Windows.Forms.ToolStripMenuItem
            {
                Text = "ClickyPathy",
                Image = Properties.Resources.icon
            };

            handleData.Click += (sender, args) => HandleData();
            menu.Items.Add(handleData);

            return menu;
        }

        private void HandleData()
        {
            try
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();

                // Convert to a list so can do list operations
                var asList = SelectedItemPaths.ToList<string>();

                if(asList.Count == 1)
                {
                    sb.Append(asList[0]);
                } else {
                    foreach(string path in asList)
                        sb.Append(path + ";");
                }

                var allPaths = sb.ToString();
                System.Windows.Forms.Clipboard.SetText(allPaths);
           }
            catch (System.Exception e)
            {
                System.Diagnostics.EventLog.WriteEntry("ClickyPathy",
                    e.StackTrace, System.Diagnostics.EventLogEntryType.Error);
            }

        }

    }
}
